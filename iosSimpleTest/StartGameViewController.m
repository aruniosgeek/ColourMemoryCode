//
//  StartGameViewController.m
//  iosSimpleTest
//
//  Created by Synergy on 1/3/16.
//  Copyright (c) 2016 TestApp. All rights reserved.
//

#import "StartGameViewController.h"
#import "UserDataStore.h"

#import "GameImage.h"

@interface StartGameViewController ()

@end

@implementation StartGameViewController
@synthesize  lastSelectedIndexPath;
@synthesize  lastFlipped;
@synthesize  gameOver;
@synthesize finishedCounter;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Loding the images
    images = [GameImage loadShuffledImages];
    self.lastSelectedIndexPath = nil;
    lastFlipped = -1;
    self.points = 0;
    self.lblFinalScoreMsg.text=@"";
    [self.lblFinalScoreMsg setHidden:YES];
    self.finishedCounter=0;
    self.scoreBoard.text = @"0";
    gameOver=YES;
       
   
    
   
    self.view.backgroundColor= [UIColor  colorWithRed:0.94 green:0.97 blue:0.99 alpha:1];
    
    self.collectionView.bounces = YES;
    [self.collectionView setShowsHorizontalScrollIndicator:NO];
    [self.collectionView setShowsVerticalScrollIndicator:NO];
    
    
    CGRect mainRect = [[UIScreen mainScreen] bounds];
    CGFloat mainWidth = mainRect.size.width;
    CGFloat mainHeight = mainRect.size.height;
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake( ((mainWidth-60.0f)/4.0f),(mainHeight-100.0f-57.0f)/4.0f);
    flowLayout.minimumLineSpacing = 10.0f;// ((mainHeight-4*(flowLayout.itemSize.height))-57.0)/3.0f;
    flowLayout.minimumInteritemSpacing = 10.0f;
    self.collectionView.collectionViewLayout = flowLayout;
    
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.titleView.bounds];
    self.titleView.layer.masksToBounds = NO;
    self.titleView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.titleView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.titleView.layer.shadowOpacity = 0.5f;
    self.titleView.layer.shadowPath = shadowPath.CGPath;
    
    
   
    
    self.titleView.backgroundColor= [UIColor colorWithRed:0.84 green:0.92 blue:0.97 alpha:1];
    
  
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
  
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return MATRIX_SIZE;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
   
    GameImage *gridImageView = (GameImage *)[cell viewWithTag:100];
    
    
    
    
    GameImage *image =[images objectAtIndex:indexPath.item];
    image = [gridImageView initWithObj:image];

    CATransform3D rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, .0, 0.5, 0.5);
    cell.contentView.alpha = 0.8;
    cell.contentView.layer.transform = rotation;
    cell.contentView.layer.anchorPoint = CGPointMake(0, 0.5);
    
    [UIView animateWithDuration:.5
                     animations:^{
                         cell.contentView.layer.transform = CATransform3DIdentity;
                         cell.contentView.alpha = 1;
                         cell.contentView.layer.shadowOffset = CGSizeMake(0, 0);
                     } completion:^(BOOL finished) {
                     }];
    
    
    
    return cell;
    
    
}



#pragma mark <UICollectionViewDelegate>




- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell* cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    GameImage *gridImage = (GameImage*)[cell viewWithTag:100];
    
    BOOL resetIndex = NO;
    
    
    if(gridImage.matched != YES && gridImage.opened != YES)
    {
        if(lastSelectedIndexPath == nil)
        {
            if(!gridImage.opened && !gridImage.matched)
                [gridImage flipInCell:cell competion:nil];
        }
        else
        {
            UICollectionViewCell * lastCell = [collectionView cellForItemAtIndexPath:lastSelectedIndexPath];
            GameImage *lastImage = (GameImage*)[lastCell viewWithTag:100];

            NSLog(@"Last index id:%ld, image :%ld", lastSelectedIndexPath.row, lastImage.imageId);
            if(lastSelectedIndexPath != indexPath)
            {
                
                
                if(lastImage.imageId == gridImage.imageId)
                {
                    self.points += 2;
                    finishedCounter +=2;
                    [gridImage flipInCell:cell competion:^{
                        [gridImage clearImage];
                        [lastImage clearImage];
                       
                    }];
                    
                    if (finishedCounter==MATRIX_SIZE) {
                        
                        [self.lblFinalScoreMsg setHidden:NO];
                       
                        self.rank = [UserDataStore getMyRank:self.points];
                        
                        self.lblFinalScoreMsg.text = [NSString stringWithFormat:@"Your Score is %ld with Rank %@",self.points,self.rank];
                        [self showAlert:@"Congratulations! Please enter your username"];
                        
                    }
                }
                else
                {
                    self.points -= 1;
                    [gridImage flipInCell:cell competion:^{
                    
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1* NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            [gridImage flopInCell:cell competion:nil];
                            [lastImage flopInCell:lastCell competion:nil];
                        
                       });
                        
                    }];
                    
                }
                resetIndex = YES;
                [self.scoreBoard setText:[NSString stringWithFormat:@"%ld",self.points]];
            }

        }
        if(resetIndex)
        {
            lastSelectedIndexPath = nil;
        }
        else
        {
            lastSelectedIndexPath = [indexPath copy];
        }
    }
    
  
    
}


-(void)showAlert:(NSString*)alertMessage
{
    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Colour Memory" message:alertMessage delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert addButtonWithTitle:@"Ok"];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSLog(@"Button Index =%ld",buttonIndex);
    if (buttonIndex == 0) {  //Login
        UITextField *username = [alertView textFieldAtIndex:0];
        NSLog(@"username: %@", username.text);
        
        
       
        
        if(username.text.length == 0 || [username.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceCharacterSet].length==0)
        {
            NSLog (@"Username is not being entered");
            
            [self showAlert:@"Please enter correct username"];
            
        }
        else
        {
            if([self isValidUserName:username.text])
            {
                
                 NSLog (@"Username Field has Valid data ");
                
                [self saveScore:username.text withPoints:self.points withRank:self.rank];

            }
            else
            {
                
                
                [self showAlert:@"Please enter correct username"];
            }

            NSLog (@"Username Field has some data ");
        }
    }
}



-(BOOL) isValidUserName:(NSString*)userName{
   
    NSError * error;
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:@"^[a-zA-Z0-9 ]{4,10}$"
                                  options:NSRegularExpressionCaseInsensitive
                                  error:&error];
    
    NSTextCheckingResult *match = [regex firstMatchInString:userName
                                                    options:0
                                                      range:NSMakeRange(0, [userName length])];
    
    BOOL isValid = match != nil;
    NSLog(@"is ValidUser Name :%d",isValid);
    
    return isValid;
}

-(void)saveScore:(NSString*)username withPoints:(NSInteger)points withRank:(NSNumber*)rank{
    
   
    NSLog(@"Saving the socre as %ld for user %@",(long)points,username);
   
    NSMutableDictionary *scoreCard=[[NSMutableDictionary alloc]init];

    [scoreCard setValue:username forKey:@"userName"];
    [scoreCard setValue:[NSNumber numberWithInteger:points] forKey:@"score"];
    [scoreCard setValue:self.rank forKey:@"rank"];
    
    [UserDataStore saveData:scoreCard withEntity:@"HighScores"];
    
    
    
    
}









@end
