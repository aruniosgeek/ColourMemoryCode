//
//  StartGameViewController.h
//  iosSimpleTest
//
//  Created by Synergy on 1/3/16.
//  Copyright (c) 2016 TestApp. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface StartGameViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UIAlertViewDelegate>
{
    NSArray * images;
}
@property (strong, nonatomic) IBOutlet UIView *titleView;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(nonatomic,assign) NSArray *gridImages;

@property (strong, atomic) IBOutlet UILabel *scoreBoard;
@property (strong, nonatomic) IBOutlet UILabel *lblFinalScoreMsg;
@property (strong, nonatomic) IBOutlet UIButton *btnHighScores;


@property (atomic)  NSInteger points;
@property (atomic)  NSNumber *rank;

@property (atomic)  NSIndexPath * lastSelectedIndexPath;
@property (atomic)  NSInteger lastFlipped;
@property (atomic)  BOOL gameOver;
@property (atomic)  NSInteger finishedCounter;

@end
