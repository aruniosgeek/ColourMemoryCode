//
//  UserDataStore.m
//  TodayLuckyNumber
//
//  Created by Synergy on 27/1/16.
//  Copyright (c) 2016 ArunApp. All rights reserved.
//

#import "UserDataStore.h"
#import  <UIKit/UIKit.h>

@implementation UserDataStore



+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

+(NSArray *) getScorecardWithRanks
{
    
    NSMutableArray * list = [UserDataStore getDataFromStore:@"HighScores"];
    NSInteger lastRank = 0;
    NSInteger lastScore = -99999999;
    
    for(int i=0;i<[list count];i++)
    {
        NSManagedObject * dict = [list objectAtIndex:i];
        
        NSNumber  * score = [dict valueForKey:@"score"];
        if(i == 0)
        {
            lastRank =1;
        }
        else
        {
            
            if(lastScore != [score integerValue])
                lastRank++;
        }

        [dict setValue:[NSNumber numberWithInteger:lastRank] forKey:@"rank"];
        lastScore = [score integerValue];

        
    }
    
    return list;
}


+(NSNumber*) getMyRank:(NSInteger)myScore
{
    
    
    
    NSArray *myScoreCards = [self getScorecardWithRanks];
    
    NSNumber * myRank  = nil;
    for(int i=0;i<[myScoreCards count];i++)
    {
        
        NSManagedObject * dict = [myScoreCards objectAtIndex:i];
        
        NSNumber *score = [dict valueForKey:@"score"];
        myRank = [dict valueForKey:@"rank"];

        if(myScore >= [score integerValue])
        {
             return myRank;
        }
        
    }
    
    myRank = [NSNumber numberWithInteger:[myRank integerValue]+1];
 
    return myRank;
    
}




+(void) prepareDummyData
{
    for(int i=0;i<100;i++)
    {
        if (i > 10 && i< 20) {
            continue;
        }
        NSDictionary * dict = @{@"userName":@"Arun",@"score":[NSNumber numberWithInteger:i],@"rank":[NSNumber numberWithInteger:0]};
        [UserDataStore saveData:dict withEntity:@"HighScores"];
    }
}

+ (BOOL)saveData:(NSDictionary *)scoreCard withEntity: (NSString *)entityNameValue {
   
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    NSManagedObject *newData = [NSEntityDescription insertNewObjectForEntityForName:entityNameValue inManagedObjectContext:context];
    
    [newData setValue:[scoreCard valueForKey:@"userName"] forKey:@"userName"];
    [newData setValue:[scoreCard valueForKey:@"score"]  forKey:@"score"];
    [newData setValue:[scoreCard valueForKey:@"rank"] forKey:@"rank"];
   
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        return NO;
    }

    
    NSLog(@"Saved Successfully! in User Data Store");
    
    return YES;
   
    
}

+(id) getDataFromStore:(NSString*)dataKey{

    
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
       
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:dataKey];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"score"
                                                                   ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSArray *fetchedObjects = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    if (fetchedObjects == nil) {
        // Handle the error
    }
    
  //  NSArray *propertiesToFetch = @[entity.propertiesByName[@"category"]];
    return fetchedObjects;

    
}




-(void)textFieldDidChange :(UITextField *)theTextField{
    NSLog( @"text changed: %@", theTextField.text);
}





@end
