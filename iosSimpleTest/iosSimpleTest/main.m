//
//  main.m
//  iosSimpleTest
//
//  Created by Synergy on 29/2/16.
//  Copyright (c) 2016 TestApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
