//
//  GameImage.h
//  iosSimpleTest
//
//  Created by Synergy on 2/3/16.
//  Copyright (c) 2016 TestApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#define  MATRIX_SIZE 4*4

@interface GameImage : UIImageView

@property (assign,atomic) BOOL opened;
@property (assign,atomic) BOOL matched;
@property (assign ,atomic) NSInteger imageId;
@property (assign ,atomic) NSInteger imagePos;
@property (atomic) NSNumber *finishedCounter;

-(id) initWithImageId:(NSInteger)imageId;
-(id) initWithObj:(GameImage*)obj;

+(NSArray *) loadShuffledImages;
-(UIImage *) getFlippedImage;
-(UIImage *) getDefaultImage;
-(void) flipInCell:(UICollectionViewCell*)cell competion:(void (^)(void))callback;
-(void) flopInCell:(UICollectionViewCell*)cell competion:(void (^)(void))callback;

-(void) clearImage;

@end
