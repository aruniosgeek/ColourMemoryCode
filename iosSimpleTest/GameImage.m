//
//  GameImage.m
//  iosSimpleTest
//
//  Created by Synergy on 2/3/16.
//  Copyright (c) 2016 TestApp. All rights reserved.
//

#import "GameImage.h"
#include <stdlib.h>
// #import "GameConstants.m"


@implementation GameImage
static int imageCounter = 1;


-(id) initWithObj:(GameImage*)obj
{
    self  = [self initWithImageId:obj.imageId];
    return self;
}
-(id) initWithImageId:(NSInteger)imageId
{
    
    NSString *imageName = [NSString stringWithFormat:@"card_bg.png"];
    UIImage * image = [UIImage imageNamed:imageName];
    self = [super initWithImage:image];
    if(self)
    {
        self.matched = NO;
        self.opened = NO;
        self.imageId = imageId;
        self.imagePos = imageCounter++;
        
    }

    return self;
}

-(UIImage *) getDefaultImage
{
    
    NSString *imageName = [NSString stringWithFormat:@"card_bg.png"];
    UIImage * image = [UIImage imageNamed:imageName];
    return image;

    
}
-(UIImage *) getFlippedImage
{
    NSString *imageName = [NSString stringWithFormat:@"colour%ld.png",self.imageId];
    NSLog(@"image Name :%@",imageName);
    UIImage * image = [UIImage imageNamed:imageName];
    return image;

}

-(void) clearImage
{
    self.opened = YES;
    self.matched  = YES;
    [UIView transitionWithView:self duration:0.1
                       options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                           self.image = nil;
                       } completion:^(BOOL finished){
                       
                           if (finished) {
                               
                               
                               
                               
                           }
                       
                       
                       }
     
     ];
}

-(void) flipInCell:(UICollectionViewCell*) cell competion:(void (^)(void))callback
{

//    [self setImage:[self getFlippedImage]];

    NSLog(@"### Flipping");
    self.opened = YES;
    self.matched  = NO;
    self.contentMode = UIViewContentModeScaleAspectFit;
    
    
    [UIView transitionWithView:self duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                           self.image = [self getFlippedImage];
                       }
                    completion:^(BOOL finished){
                        if (finished && callback) {
                            callback();
                        }
                    }
     
     ];
    
    
 
    
}
-(void) flopInCell:(UICollectionViewCell*)cell competion:(void (^)(void))callback
{
    
    self.opened = NO;
    self.matched  = NO;
    self.contentMode = UIViewContentModeScaleAspectFit;
    [UIView transitionWithView:self duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
                           self.image = [self getDefaultImage];
                       }
                    completion:^(BOOL finished){
                        if (finished && callback){
                            callback();
                        }
                    }
     
     ];
    

    
}
+(NSArray *) loadShuffledImages
{
    
    imageCounter = 0;

    NSMutableArray *images = [[NSMutableArray alloc] init] ;
    for (int i = 0; i < MATRIX_SIZE/2; i++)
    {
        GameImage *image = [[GameImage alloc] initWithImageId:i+1];
        GameImage *pair = [[GameImage alloc] initWithImageId:i+1];
        [images addObject:image];
        [images addObject:pair];
    }
    
    for (int x = 0; x < [images count]; x++) {
        int randInt = (arc4random() % ([images count] - x)) + x;
        [images exchangeObjectAtIndex:x withObjectAtIndex:randInt];
    }
    
    return images;
}

@end
