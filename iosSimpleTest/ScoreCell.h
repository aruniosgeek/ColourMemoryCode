//
//  ScoreCell.h
//  iosSimpleTest
//
//  Created by Synergy on 6/3/16.
//  Copyright (c) 2016 TestApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *lblUserName;


@property (strong, nonatomic) IBOutlet UILabel *lblScore;

@property (strong, nonatomic) IBOutlet UILabel *lblRank;

@end
