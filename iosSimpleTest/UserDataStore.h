//
//  UserDataStore.h
//  TodayLuckyNumber
//
//  Created by Synergy on 27/1/16.
//  Copyright (c) 2016 ArunApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface UserDataStore : NSObject


+ (BOOL)saveData:(NSDictionary *)info withEntity: (NSString *)entityNameValue;
+(id) getDataFromStore:(NSString*)dataKey;
+(NSArray *) getScorecardWithRanks;
+(void) prepareDummyData;
+(NSNumber*) getMyRank:(NSInteger)myScore;

@end
