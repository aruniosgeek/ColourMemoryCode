//
//  HighestScoreCard.m
//  iosSimpleTest
//
//  Created by Synergy on 6/3/16.
//  Copyright (c) 2016 TestApp. All rights reserved.
//

#import "HighestScoreCard.h"

#import <QuartzCore/QuartzCore.h>

#import "ScoreCell.h"
#import "UserDataStore.h"
#import  "StartGameViewController.h"

@interface HighestScoreCard (){
    NSArray *highScores;
}
@end

@implementation HighestScoreCard

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}





- (void)viewDidLoad
{
    [super viewDidLoad];
    
    highScores = [UserDataStore getScorecardWithRanks];
    
    self.navigationController.navigationBar.backgroundColor=[UIColor colorWithRed:0.84 green:0.92 blue:0.97 alpha:1];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"CourierNewPS-BoldMT" size:22], NSFontAttributeName,
                                [UIColor colorWithRed:0.96 green:0.49 blue:0.15 alpha:1], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes=attributes;
    
    
    self.view.backgroundColor= [UIColor colorWithRed:0.94 green:0.97 blue:0.99 alpha:1];
    
    
 
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return [highScores count];
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //1. Get the cell
    static NSString *CellIdentifier = @"ScoreCell";
    ScoreCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[ScoreCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.backgroundColor=[UIColor colorWithRed:0.16 green:0.57 blue:0.04 alpha:1];
    }
    
    tableView.backgroundColor=[UIColor colorWithRed:0.13 green:0.5 blue:0.74 alpha:1];
    NSMutableDictionary *scoreCardObj =[highScores objectAtIndex:indexPath.row];
    
    
    NSMutableString *myscore = [NSMutableString stringWithFormat:@"%@",[scoreCardObj valueForKey:@"score"]];
    
    
    cell.lblScore.text =myscore;
    cell.lblUserName.text=[scoreCardObj valueForKey:@"userName"];
    cell.lblRank.text=[NSString stringWithFormat:@"%@",[scoreCardObj valueForKey:@"rank"]];
    
    
    return cell;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    
    return @"";
}













@end
